FROM ubuntu:xenial

MAINTAINER Ádám Keserű <keseru.adam@gmail.com>

RUN set -xe \
# installing required ubuntu packages
      && apt-get update && apt-get install -y --no-install-recommends ssh git python-pip mailutils ssmtp \
# cleanup
      && apt-get clean \
      && rm -rf /var/lib/apt/lists/*

RUN set -xe \
# installing required python packages
      && pip install --no-cache-dir setuptools \
      && pip install --no-cache-dir mock coverage green \
# cleanup 
      && rm -rf ~/.cache/pip

# setting up 'git gitlab'
RUN set -xe \
# installing required python packages
      && pip install --no-cache-dir libsaas \
# cloning 'git-gitlab' and requirements
      && cd /opt/ \
      && git clone https://gitlab.com/bor-sh-infrastructure/libsaas_gitlab.git \
      && git clone https://gitlab.com/Rogerius/git-gitlab \
# installing 'git-gitlab' and requirements
      && cd libsaas_gitlab \
      && python setup.py install \
      && cd ../git-gitlab \
      && python setup.py install \
# adding alias for 'git-gitlab'
      && git config --global alias.gitlab lab \
# unsetting proxy
      && unset http_proxy \
      && unset https_proxy \
# cleanup
      && cd /opt/ \
      && rm -rf libsaas_gitlab \
      && rm -rf git-gitlab \
      && rm -rf ~/.cache/pip

CMD ["/bin/bash"]
